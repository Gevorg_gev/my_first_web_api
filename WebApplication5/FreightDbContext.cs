using System.Runtime.InteropServices;
using System;
using Microsoft.EntityFrameworkCore;

namespace WebApplication5
{
    public class FreightDbContext : DbContext 
    {
        public DbSet<Freight> Freights { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=LAPTOP-R91MV32G;Database=FreightDb;Trusted_Connection=True;");
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Freight>(x=>
            {
                x.ToTable("Freights");
                x.HasKey(x=>x.Freight_Id);
                x.Property(x => x.Freight_Id).ValueGeneratedOnAdd();
                x.Property(x => x.Freight_Massive).HasColumnType("NVARCHAR(20)");
                x.Property(x => x.Freight_Model).HasColumnType("NVARCHAR(30)");
                x.Property(x => x.Freight_Name).HasColumnType("NVARCHAR(50)");
                x.Property(x => x.Freight_Country).HasColumnType("NVARCHAR(20)");
                x.Property(x => x.Freight_City).HasColumnType("NVARCHAR(30)"); 
            });
            base.OnModelCreating(modelBuilder);
        }

    }
}
