﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication5.Migrations
{
    public partial class Correct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Freights",
                columns: table => new
                {
                    Freight_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Freight_Massive = table.Column<string>(type: "NVARCHAR(20)", nullable: false),
                    Freight_Name = table.Column<string>(type: "NVARCHAR(50)", nullable: true),
                    Freight_Country = table.Column<string>(type: "NVARCHAR(20)", nullable: true),
                    Freight_City = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Freight_Model = table.Column<string>(type: "NVARCHAR(30)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Freights", x => x.Freight_Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Freights");
        }
    }
}
