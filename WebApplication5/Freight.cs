using System.Runtime.InteropServices;
using System;

namespace WebApplication5
{
    public class Freight
    {
        public int Freight_Id { get; set; }
        public int Freight_Massive { get; set; }
        public string Freight_Name { get; set; }
        public string Freight_Country { get; set; }
        public string Freight_City { get; set; } 
        public string Freight_Model { get; set; } 
    }
}
