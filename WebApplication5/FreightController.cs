using System.Runtime.InteropServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication5
{
    [ApiController]
    [Route("Controller")]
    public class FreightController : ControllerBase
    {
        [HttpPost]
        public IActionResult AddFreight([FromBody] Freight freight)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            using (var dbContext = new FreightDbContext()) 
            {
                dbContext.Freights.Add(freight);
            }
                return Ok(); 
        } 

        
    }
}